#Dan Pittman
#10/26/2014
#This script launches chrome and all of the tabs associated with APPTHWACK
############################################################################


#imports webbroswer library
import subprocess
import webbrowser
from subprocess import call

#calls webbrowser.get which opens ('your web browser') and opens a new window with
#.open_new('url')
webbrowser.get('google-chrome').open_new('https://docs.google.com/a/trell.is/spreadsheets/d/1UWykgmP1XWafK43uv5q22Yoe4PbeH8D2s1_HrkHRbxU/edit#gid=0')

#uses same function to open new tabs

webbrowser.get('google-chrome').open_new_tab('https://appthwack.slack.com/messages/dev/')
webbrowser.get('google-chrome').open_new_tab('https://10.8.2.1/appthwack/appadmin/select/db')
webbrowser.get('google-chrome').open_new_tab('https://drive.google.com/drive/u/1/#my-drive')
webbrowser.get('google-chrome').open_new_tab('https://appthwack.uservoice.com/admin/reports')
webbrowser.get('google-chrome').open_new_tab('https://appthwack.com/project/provisioning-ios')
webbrowser.get('google-chrome').open_new_tab('https://appthwack.com/project/provisioning')
webbrowser.get('google-chrome').open_new_tab('https://www.facebook.com/index.php?stype=lo&lh=Ac_2W6Gaf3lyNYTN')

#opens a terminal custom terminal session
subprocess.call(['terminator', '-T APPTHWACK', '-m'])
