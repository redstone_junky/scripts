import csv

def list():
    device_list = []
    with open("android.csv", 'rb') as csvfile:
        devicereader = csv.reader(csvfile, delimiter= ',', quotechar="\"")
        keys = get_headers()
        for row in devicereader:
            device = AndroidDevice()
            for index, key in enumerate(keys):
                device[key] = row[index]
            if device["serial_number"] != "" and device["ip"] != "RETIRED":
              device_list.append(device)
        device_list.pop(0)
        return device_list

def get_headers():
    with open("android.csv", 'rb') as csvfile:
        device_reader = csv.reader(csvfile, delimiter= ',', quotechar="\"")
        # Todo -> .lower().replace(" ", "_") on each string in the returned array.
        return map(lower_score, device_reader.next())

def lower_score(that_string):
    return that_string.lower().replace(" ", "_")

class AndroidDevice(dict):
    """
    An object to store information about a device in the device spreadsheet
    """
    def __init__(self,*arg,**kw):
        super(AndroidDevice, self).__init__(*arg, **kw)
        

    
