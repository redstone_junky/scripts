import devices
import subprocess

devices_list = devices.list()
good_devices = []
bad_devices = []

for device in devices_list:
  command = ["ssh", device["hostname"], "adb -s", device["serial_number"], "get-state"]
  result = subprocess.check_output(command).strip()
  if result == "unknown":
    bad_devices.append(device)
    print "~" * 80
    print "Could not connect to device: ", device["device"]
    print "AppThwack ID:\t", device["appthwack_id"]
    print "Host:\t", device["hostname"]
    print "Serial:", device["serial_number"]
    print "Rack:\t",  device["rack"]
    print "Shelf:\t", device["shelf"]
    print "Color:\t",  device["color"]
  else:
    good_devices.append(device)
