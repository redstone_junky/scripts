require 'json'
require 'pathname'
require 'fileutils'

# Which projects should we get logs for?
projects = ["25546", "25175"]
projects.each do |project|
  puts "Fetching results for project: #{project}"
  page_number = 0
  result_page = `curl -s -G -u "BLSWS4bBeh73s8XdcjvdEAh08v7QUac1n4A7pD7z:" https://appthwack.com/api/run/#{project} -d "page=#{page_number}"`

  results = JSON.parse(result_page)
  total_pages = /page=(\d*)/.match(results["last_page"])[1].to_i

  while page_number <= total_pages do
    result_page = `curl -s -G -u "BLSWS4bBeh73s8XdcjvdEAh08v7QUac1n4A7pD7z:" https://appthwack.com/api/run/#{project} -d "page=#{page_number}"`
    results = JSON.parse(result_page)
    if ! File.exists?("appthwack_stats") then
      FileUtils.mkdir("appthwack_stats")
    end
    file_path = "appthwack_stats/" + project + "-" + page_number.to_s
    file_1 = File.new(file_path, "w+") 
    file_1.write(result_page)
    file_1.close
    page_number += 1
  end
end

