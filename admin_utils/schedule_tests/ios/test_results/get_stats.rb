require 'json'
require 'pathname'
require 'fileutils'
require 'yaml'

files = Dir["appthwack_stats/*"]
start_at_run_number = 
runs = []
pass_count = 0
warning_count = 0
unknown_count = 0


#runs #last_page #previous_page #total_count #first_page #next_page
files.each do |file|
  contents = JSON.parse(File.open(file).read)
  contents["runs"].each do |run|
    if /\d*-(\d*).zip/.match(run["name"])
      runs << run
    end
  end
end

runs.each do |run|
  stat = {
    :id => run["id"],
    :result => run["result"],
    :minutes => run["minutes_used"],
    :status_text => run["status_text"],
    :name => run["name"],
  }
  case stat[:result]
  when "pass"
    pass_count += 1
  when "warning"
    warning_count += 1
  when "unknown"
    unknown_count += 1
  end
end


puts "Total runs: #{runs.length}"
puts "Pass: #{(pass_count * 100 / runs.length.to_f).round(2)}%" + "\tWarning: #{(warning_count * 100 / runs.length.to_f).round(2)}%" + "\tUnknown: #{(unknown_count * 100 / runs.length.to_f).round(2)}%"
puts "Pass: #{pass_count}\tWarning: #{warning_count}\tUnknown: #{unknown_count}"

#run_file = File.new("runs.log", "w+")
#run_file.write(runs)
#run_file.close

