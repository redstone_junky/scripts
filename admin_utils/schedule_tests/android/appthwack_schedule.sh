#Open parameter list file
while IFS='=' read param value
do
  #Ignore lines starting with '#'
  [[ "$param" =~ ^#.*$ ]] && continue

  #Remove leading and trailing whitespace in param and values
  param=`echo $param | sed -e 's/^ *//' -e 's/ *$//'`
  value=`echo $value | sed -e 's/^ *//' -e 's/ *$//'`

  #Ignore blank params and values
  [[ -z $param ]] && continue
  [[ -z $value ]] && continue

  #Remove single or double quotes around values, if present
  value=`echo $value | sed -e "s/^'//" -e "s/'$//"`
  value=`echo $value | sed -e 's/^"//' -e 's/"$//'`
  echo "PARAM =" $param "\tVALUE =" $value

  case $param in
    api_key) api_key=$value;; 
    app_name) app_name=$value;;
    app_path) app_path=$value;;
    archive_name) archive_name=$value;;
    archive_path) archive_path=$value;;
    project_id) project_id=$value;;
    run_name) run_name=$value;;
    pool_id) pool_id=$value;;
    framework) framework=$value;;
    *) echo "ERROR: unexpected param in input file"; exit;;
  esac
  
done < "appthwack_info.txt"

app_str="$(curl -X POST -u "$api_key" https://appthwack.com/api/file -F "name=$app_name" -F "file=@$app_path")"
app_id=$(echo "$app_str" | grep -Eow "[0-9]+")
echo "APP ID = $app_id"

zip_str="$(curl -X POST -u "$api_key" https://appthwack.com/api/file -F "name=$archive_name" -F "file=@$archive_path")"
zip_id=$(echo "$zip_str" | grep -Eow "[0-9]+")
echo "ZIP ID = $zip_id"

curl -X POST -u "$api_key" https://appthwack.com/api/run -F "project=$project_id" -F "name=$run_name" -F "app=$app_id" -F "pool=$pool_id" -F "$framework=$zip_id"


