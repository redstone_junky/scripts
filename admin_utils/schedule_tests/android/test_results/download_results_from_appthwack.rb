require 'json'
require 'pathname'
require 'fileutils'

# Which projects should we get logs for?
projects = ["25546", "25175"]

logging_path = {}
Dir["./*/*/*/*"].each do |run|
  if /\/(\d*)$/.match(run) then
    key = /\/(\d*)$/.match(run)[1]
    logging_path[key] = run
  end
end

runs = []

projects.each do |project|
  puts "Fetching results for project: #{project}"
  page_number = 0
  result_page = `curl -s -G -u "BLSWS4bBeh73s8XdcjvdEAh08v7QUac1n4A7pD7z:" https://appthwack.com/api/run/#{project} -d "page=#{page_number}"`

  results = JSON.parse(result_page)
  total_pages = /page=(\d*)/.match(results["last_page"])[1].to_i

  while page_number <= total_pages do
    result_page = `curl -s -G -u "BLSWS4bBeh73s8XdcjvdEAh08v7QUac1n4A7pD7z:" https://appthwack.com/api/run/#{project} -d "page=#{page_number}"`
    results = JSON.parse(result_page)
    puts "Checking page number: #{page_number.to_s}/#{total_pages}"
    results["runs"].each do |run| 
      runs << [run["name"], run["id"].to_s]
    end
    page_number += 1
  end
end

# runs[n][0] => Filename
# runs[n][1] => AppThwack run id
runs.each do |run|
  if /(\d*)-\d*.zip/.match(run[0]) then
    project    = /(\d*)-\d*.zip/.match(run[0])[1]
  end
  if /\d*-(\d*).zip/.match(run[0])
    jenkins_id = /\d*-(\d*).zip/.match(run[0])[1]
  end
  if ! project.nil? and ! jenkins_id.nil? then
    if logging_path[jenkins_id] then
      appthwack_logs = Pathname.new(logging_path[jenkins_id]).join("appthwack_logs")
      if ! File.exists?(appthwack_logs) then
        FileUtils.mkdir_p(appthwack_logs) 
        puts "Getting results for project, #{project} with run id #{run[1]}"
        `curl -X GET -L -u "BLSWS4bBeh73s8XdcjvdEAh08v7QUac1n4A7pD7z:" https://appthwack.com/api/run/#{project}/#{run[1]}?format=archive -o #{appthwack_logs.join("#{project}-#{jenkins_id}")}.zip`
      elsif Dir[appthwack_logs.join("*")].index{|s| s =~ /zip/i} then 
        puts "Trying again to get results for project, #{project} with run id #{run[1]}"
          `curl -X GET -L -u "BLSWS4bBeh73s8XdcjvdEAh08v7QUac1n4A7pD7z:" https://appthwack.com/api/run/#{project}/#{run[1]}?format=archive -o #{appthwack_logs.join("#{project}-#{jenkins_id}")}.zip`
      else
        puts "Skipping project, #{project}, because we already downloaded those results."
      end
    else
      puts "The logging path couldn't be determined. The Jenkins ID is: #{jenkins_id}"
    end
  else
    puts "Project and jenkins_id == false"
    puts "run[0] => #{run[0]}"
    puts "/(\d*)-\d*.zip/.match(run[0]) => #{/(\d*)-\d*.zip/.match(run[0])}"
    puts "/\d*-(\d*).zip/.match(run[0]) => #{/\d*-(\d*).zip/.match(run[0])}"
  end
end
